package com.example.khawoat_rmbp.maidxcellent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ConfigurationHelper;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.facebook.AppEventsLogger;
import com.facebook.FacebookActivity;
import com.facebook.FacebookSdk;
import com.facebook.Session;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

public class MainAfterLogin extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_main_after_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        View headerLayout = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView profileView = (ImageView)headerLayout.findViewById(R.id.imageView);
        TextView profileName = (TextView)headerLayout.findViewById(R.id.profile_name);
        TextView profileUserEmail = (TextView)headerLayout.findViewById(R.id.textView);
       String profileUserEMAIL = Config.PROFILE_EMAIL;
  //     String profileFirstName = returnValueFromBundles(Config.PROFILE_FIRST_NAME);
//        String profileLastName = returnValueFromBundles(Config.PROFILE_LAST_NAME);
 //       String profileImageLink = returnValueFromBundles(Config.PROFILE_IMAGE_URL);
//       profileName.setText(profileFirstName + " " + profileLastName);
        profileName.setText(Config.PROFILE_FIRST_NAME);
           profileUserEmail.setText(Config.PROFILE_EMAIL);
 //       profileUserId.setText("User ID : " + profileUserID);
        Picasso.with(MainAfterLogin.this).load(Config.PROFILE_IMAGE_URL).into(profileView);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            LoginManager.getInstance().logOut();
            Intent logoutIntent = new Intent(MainAfterLogin.this, FacebookActivity.class); startActivity(logoutIntent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.new_Booking) {
            // Handle the camera action
        } else if (id == R.id.my_Booking) {

        } else if (id == R.id.notification) {

        } else if (id == R.id.setting) {

        } else if (id == R.id.logout) {
            LoginManager.getInstance().logOut();
            Intent i = new Intent(MainAfterLogin.this, MainActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private String returnValueFromBundles(String key){
        Bundle inBundle = getIntent().getExtras();
        String returnedValue = inBundle.get(key).toString();
        return returnedValue;
    }


}
