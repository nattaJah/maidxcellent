package com.example.khawoat_rmbp.maidxcellent.Util;

/**
 * Created by KHAWOAT-rMBP on 6/21/2017 AD.
 */

public class Config {
    public static String PROFILE_USER_ID = "USER_ID";
    public static String PROFILE_FIRST_NAME = "PROFILE_FIRST_NAME";
    public static final String PROFILE_LAST_NAME = "PROFILE_LAST_NAME";
    public static String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";
    public static String PROFILE_EMAIL = "PROFILE_EMAIL";
}
