package com.example.khawoat_rmbp.maidxcellent;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.khawoat_rmbp.maidxcellent.Util.Config;
import com.example.khawoat_rmbp.maidxcellent.Util.CustomVolleyRequest;
import com.facebook.AppEventsLogger;
import com.facebook.CallbackManager;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener{

    private CallbackManager callbackManager;

    private TextView userName;

    private LoginButton loginBtn;

    private UiLifecycleHelper uiHelper;

    private ImageView imgProfile;

    private SignInButton signInButton;

    private GoogleSignInOptions gso;

    private GoogleApiClient mGoogleApiClient;

    private int RC_SIGN_IN = 100;

    private MainActivity mContext;

    private ImageLoader imageLoader;

    private NetworkImageView profilePhoto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = MainActivity.this;
        callbackManager = CallbackManager.Factory.create();
        uiHelper = new UiLifecycleHelper(this, statusCallback);
        uiHelper.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
      //  profilePhoto = (NetworkImageView) findViewById(R.id.profileImage);

        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing signinbutton
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(this, mContext)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing signinbutton
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        showHashKey(this);
        bindView();
        setView();
//        loginBtn.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
//            @Override
//            public void onUserInfoFetched(GraphUser user) {
//                if (user != null) {
//                    userName.setText("Hello, " + user.getName());
//                    String profile_url = "https://graph.facebook.com/" +  user.getId() + "/picture?type=large";
//                    String firstName = user.getName();
//                    String email = user.getEmail();
//                    Config.PROFILE_IMAGE_URL = profile_url;
//                    Config.PROFILE_FIRST_NAME = firstName;
//                    Config.PROFILE_EMAIL = email;
//                    Intent facebookIntent = new Intent(MainActivity.this, MainAfterLogin.class);
//                    facebookIntent.putExtra(Config.PROFILE_IMAGE_URL, profile_url);
//                    facebookIntent.putExtra(Config.PROFILE_FIRST_NAME, firstName);
//                    facebookIntent.putExtra(Config.PROFILE_EMAIL, email);
//                    startActivity(facebookIntent);
//                    //Picasso.with(MainActivity.this).load(profile_url).into(imageProfile);
//
//                } else {
//                    userName.setText("You are not logged");
//
//
//                }
//            }
//        });



    }

    private void bindView(){
//        imgProfile = (ImageView) findViewById(R.id.ivProfile);
        userName = (TextView) findViewById(R.id.user_name);
        loginBtn = (LoginButton) findViewById(R.id.authButton);
    }

    private void setView(){
        loginBtn.setOnClickListener(this);
        // Google-Signin
        signInButton.setOnClickListener(this);
        userName.setText("กรุณา Login");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.authButton:
                loginBtn.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
                    @Override
                    public void onUserInfoFetched(GraphUser user) {
                        if (user != null) {
                            userName.setText("Hello, " + user.getName());
                            String profile_url = "https://graph.facebook.com/" +  user.getId() + "/picture?type=large";
                            String firstName = user.getName();
                            String email = user.getEmail();
                            Config.PROFILE_IMAGE_URL = profile_url;
                            Config.PROFILE_FIRST_NAME = firstName;
                            Config.PROFILE_EMAIL = email;
                            Intent facebookIntent = new Intent(MainActivity.this, MainAfterLogin.class);
                            facebookIntent.putExtra(Config.PROFILE_IMAGE_URL, profile_url);
                            facebookIntent.putExtra(Config.PROFILE_FIRST_NAME, firstName);
                            facebookIntent.putExtra(Config.PROFILE_EMAIL, email);
                            startActivity(facebookIntent);
                            //Picasso.with(MainActivity.this).load(profile_url).into(imageProfile);

                        } else {
                            userName.setText("You are not logged");


                        }
                    }
                });
                break;
            case R.id.sign_in_button:
                signIn();
                break;

        }
    }

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
            uiHelper.onActivityResult(requestCode, resultCode, data);
        }

        }


    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            //Displaying name and email
                //textViewName.setText(acct.getDisplayName());
                //textViewEmail.setText(acct.getEmail());

            //Initializing image loader
            imageLoader = CustomVolleyRequest.getInstance(this.getApplicationContext())
                    .getImageLoader();

//            imageLoader.get(acct.getPhotoUrl().toString(),
//                    ImageLoader.getImageListener(profilePhoto,
//                            R.mipmap.ic_launcher,
//                            R.mipmap.ic_launcher));

            String Profile_Picture_GG = acct.getPhotoUrl().toString();
            String nameGG = acct.getDisplayName().toString();
            String emailGG = acct.getEmail().toString();
            Config.PROFILE_IMAGE_URL = Profile_Picture_GG;
            Config.PROFILE_FIRST_NAME = nameGG;
            Config.PROFILE_EMAIL = emailGG;
            Intent googleIntent = new Intent(MainActivity.this,MainAfterLogin.class);
            startActivity(googleIntent);

            //Loading image
           // profilePhoto.setImageUrl(acct.getPhotoUrl().toString(), imageLoader);

        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }


//    private void Login(){
//        loginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        //                         String userLoginId = loginResult.getAccessToken().getUserId().toString();
//
//                        Profile mProfile = Profile.getCurrentProfile();
//                        String firstName = mProfile.getFirstName();
//                        String lastName = mProfile.getLastName();
//                        String userId = mProfile.getId().toString();
//                        Config.PROFILE_IMAGE_URL = mProfile.getProfilePictureUri(96, 96).toString();
//                        openMainAfterLogin(userId,firstName,lastName,Config.PROFILE_IMAGE_URL);
//
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                    }
//                });
//    }
//
//    private void openMainAfterLogin(String userId, String firstName, String lastName, String profileImageUrl){
//        Intent facebookIntent = new Intent(MainActivity.this, MainAfterLogin.class);
//        facebookIntent.putExtra(Config.PROFILE_USER_ID, userId);
//        facebookIntent.putExtra(Config.PROFILE_FIRST_NAME, firstName);
//        facebookIntent.putExtra(Config.PROFILE_LAST_NAME, lastName);
//        facebookIntent.putExtra(Config.PROFILE_IMAGE_URL, profileImageUrl);
//        startActivity(facebookIntent);
//    }


    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.example.khawoat_rmbp.maidxcellent", PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i("KeyHash:", "error");
        } catch (NoSuchAlgorithmException e) {
            Log.i("KeyHash:", "error");
        }
    }

    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            if (state.isOpened()) {
                Log.d("FacebookSampleActivity", "Facebook session opened");
            } else if (state.isClosed()) {
                Log.d("FacebookSampleActivity", "Facebook session closed");
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//        uiHelper.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        uiHelper.onSaveInstanceState(savedState);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}